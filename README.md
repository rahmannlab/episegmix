# EpiSegMix: A Flexible Distribution Hidden Markov Model with Duration Modeling for Chromatin State Discovery

This repository contains a Snakemake workflow for chromatin segmentation to annotate both coding and non-coding regions of the genome.

EpiSegMix first estimates the parameters of a hidden Markov model, where each state corresponds to a different combination of epigenetic modifications and thus represents a functional role, such as enhancer, transcription start site, active or silent gene. The spatial relations are captured via the transition probabolities. After the parameter estimation, each region in the genome is annotated with the most likely chromatin state. The implementation allows to choose for each histone modification a different distributional assumption (the available distributions are listed below). Similar tools are ChromHMM or EpiCSeg (references [2] and [3]).

The implementation of the HMM is in C++. The parameters are estimated using the Baum-Welch algorithm and for decoding the user can select either the Viterbi algorithm or posterior decoding. For all distributions where the MLE contains no closed form solution, the parameters are updated by numerical optimization using the Migrad minimizer from ROOT (<https://root.cern/root/htmldoc/guides/minuit2/Minuit2Letter.pdf>). The initial parameters of the HMM are found using k-Means clustering. Each cluster is assumed to correspond to one state and the parameters are initialized using method of moment estimates.

## Installation

First, open a shell and clone the repository 


```
git clone https://gitlab.com/rahmannlab/episegmix.git
```

Change to the *episegmix* directory and continue with the installation.

For the most convenient installation, we advise to use mamba/conda and to perform the following steps (for more detailed information on using Snakemake check out <https://snakemake.readthedocs.io/en/stable/index.html>)

1. Creating the environment 

```
mamba env create --file workflow/env.yaml
```

2. Activating the environment

```
conda activate episegmix
```

3. Compiling the C++ library (requires C++ 17)

Run the bash script in the active *episegmix* environment.
```
bash build.sh
```

## Running the Workflow

To start the workflow, always activate the environment created above. 

To test the tool, exemplary data is provided in the *data* folder and an exemplary model file in the *models* folder (the config file is specified accordingly). Without further changes, the *snakemake* command can directly executed and the results can be found in the *results* folder (with duration modeling, a model file is provided in *models_topology* and the results are written to *results_topology*). 

### Workflow without Duration Modeling 

1. Adjust the *workflow/config.yaml* file. The configuration file should contain the path to the input directory with the model information, the output directory for the results, and if statistics should be plotted and bed files created ( bed files for Viterbi and posterior decoding and bedGraph for the posterior scores), e.g.

```yaml
  output_dir: path/to/results
  model_dir: path/to/models
  create_bed_files: true
  plot_statistics: true
  column: viterbi
```

All paths can either be given relative to the working directory or as absolute paths.

2. Create a YAML file in the model directory containing for each histone modification its distributional assumption as well as the path to the data files. The YAML file should be inside the specified *model_dir*, e.g. *models/8N_K562_1_DISTFIT.yaml* (for further details see below).

3. Run the Snakemake workflow

```
snakemake --cores 1
```

The parameters of the config file can also be set via the command line, e.g. for setting the boolean variable `create_bed_files` to false, call snakemake with

```
snakemake --cores 1 --config create_bed_files=false
```

For running the snakemake workflow from a different directory you can also specify the location of the Snakefile (-s) and the working directory (-d), e.g,

```
snakemake -c1 -s Documents/ChromatinSegmentationWorkflow/workflow/Snakefile -d Documents/ChromatinSegmentationWorkflow/ --config output_dir=path/to/results model_dir=path/to/models
```

If more then one model is given in the model directory, the workflows can be run in parallel by specifying more than one core

```
snakemake --cores n
```

4. The trained model parameters, the segmentation and the plots can be found in the *output_dir*, e.g. *results/8N_K562_1_DISTFIT* 

### Workflow with Duration Modeling 

The workflow with duration modeling can be run similar to the workflow without duration modeling but under specification of another Snakefile

```
snakemake --cores 1 -s workflowTopology/Snakefile 
```

The config file should look like this

```yaml
output_dir: results_topology
model_dir: models_topology
plot_statistics: true
``` 

## Supported Distributions

- Poisson (**PO**)
- Zero Adjusted Poisson (**ZAP**)
- Binomial (**BI**)
- Negative Binomial (**NBI**)
- Zero Adjusted Negative Binomial (**ZANBI**)
- Beta Binomial (**BB**)
- Beta Negative Binomial (**BNB**)
- Zero Adjsuted Beta Negative Binomial (**ZABNB**)
- Sichel (**SI**)
- Zero Adjusted Sichel (**ZASI**)
- Gaussian (**GA**)
- Bernoulli (**B**, requires binarized input)

## Example Input Files

- The count matrix should have the following format (tab separated)

| chr | start |  end | H3K36me3 | H3K9me3 | H3K27me3 | ... |
|:---:|:-----:|:----:|:--------:|:-------:|:--------:|:---:|
|  1  |  200  | 400  |     0    |    0    |     0    | ... |
|  1  |  400  | 600  |    12    |    26   |    30    | ... |
|  1  |  600  | 800  |    14    |    24   |    28    | ... |
| ... |  ...  |  ... |    ...   |   ...   |    ...   | ... |

- Example YAML file to build a HMM with 8 states for a dataset with the six core histone marks. The count matrix is located at *data/counts_K562_1.txt*. See Section **Create count matrix** for an explanation on how to create a count matrix from bam files.

```yaml
states: 8
marker: 6
marker_spec:
  - name: H3K27me3
    distribution: NBI
  - name: H3K9me3
    distribution: SI
  - name: H3K36me3
    distribution: BNB
  - name: H3K4me3
    distribution: NBI
  - name: H3K4me1
    distribution: SI
  - name: H3K27ac
    distribution: SI
data: [data/counts_K562_1.txt]
# optional: which chromosome to use for parameter estimation, default Encode pilot regions (pilot_hg38)
chr: 1
```

The path to the count matrix must be specified as a list. To train the HMM on multiple independent observations, you provide multiple files. By default, the model is fitted on the Encode pilot regions (for hg38) and the predication is performed genome wide. Alternatively, a list of chromosomes or a single chromsome can be provided for the *chr* option, then the model is trained on the specified chromosomes instead. In addition, if you set *chr: pilot_hg19*, the coordinates of the pilot regions for the human reference 19 are used. 

## Workflow for Distribution Fitting

We additionally provide a workflow that tests for each mark a list of distributions. 

To run the workflow, call

```
snakemake --cores 4 -s workflow_distribution_fitting/Snakefile
```

The following parameters can be adjusted in the config file
```yaml
output_dir: models_topology
states: 8
name: K562_1
data: data/counts_K562_1.txt
marks: [H3K27me3, H3K9me3, H3K36me3, H3K4me3, H3K4me1, H3K27ac]
distributions:  [NBI, SI, BNB]
```

For the above example, the workflow outputs a model file for an 8-state HMM using the distributions with the highest log-likelihood values. Here, testing the negative binomial, Sichel and beta negative binomial distribution.

## Create Count Matrix

To create a count matrix from a set of bam files, we provide an additional script that creates a count matrix using bamsignals [4], provided in the *get_counts/* folder. 

First, create an additional environment by calling from inside *get_counts/*

```
mamba env create --file env.yaml
```

Activate the new environment every time before you call the script

```
conda activate counts
```

To create a count matrix run the following command

```
bash counts.sh -t <files>.txt -o <output_dir> -f <name> -g hg38
```
where *<files>.txt* is a tab delimited file containing the absolute path to the bam files, e.g.

```
H3K9me3	/home/Documents/bam/ENCFF146NLP.bam
H3K27me3	 /home/Documents/bam/ENCFF190OWE.bam
H3K36me3	/home/Documents/bam/ENCFF639PLN.bam
```

A help message with further options is printed if you call

```
bash counts.sh -h
```

## Citation

For a full description and for citing use, please cite the following article on bioarxiv

> Schmitz, J. E., Aggarwal, N., Laufer, L. et al. 2023. EpiSegMix: A Flexible Distribution Hidden Markov Model with Duration Modeling for Chromatin State Discovery, <https://doi.org/10.1101/2023.09.07.556549>

## References

[1] Mölder, F., Jablonski, K.P., Letcher, B. et al. 2021. Sustainable data analysis with Snakemake. F1000Res 10, 33 <https://f1000research.com/articles/10-33>.

[2] Ernst, J., and Kellis, M. (2010). “Discovery and characterization of chromatin states for systematic annotation of the human genome,” Nature Biotechnology 28(8), 817–825, doi: <https://doi.org/10.1038/nbt.1662>.

[3] Mammana, A., and Chung, H.-R. (2015). “Chromatin segmentation based on a probabilistic model for read counts explains a large portion of the epigenome,” Genome Biology 16(1), 151, doi:<https://doi.org/10.1186/s13059-015-0708-z>.

[4] Mammana A, and Helmuth J (2023). bamsignals: Extract read count signals from bam files. R package version 1.32.0, https://github.com/lamortenera/bamsignals. 
