import argparse
import math
import numpy as np
import pandas as pd
import json
import warnings

import plots

def main():
    warnings.simplefilter(action='ignore', category=FutureWarning)

    parser = argparse.ArgumentParser(description = "Results of chromatin segmentation.")
    parser.add_argument("-c", metavar = "data", type = str, nargs = 1, help = "Data with histone modifcations and segmentation.")
    parser.add_argument("-j", metavar = "json", type = str, nargs = 1, help = "Json file with HMM parameters.")
    parser.add_argument("-t", metavar = "transtion_matrix", type = str, nargs = 1, help = "Plot transition matrix.")
    parser.add_argument("-e", metavar = "mean_emission", type = str, nargs = 1, help = "Plot mean emission.")
    parser.add_argument("-n", metavar = "norm_emission", type = str, nargs = 1, help = "Plot mean emission normalized per marker.")
    # parser.add_argument("-d", metavar = "median_emission", type = str, nargs = 1, help = "Plot median emission.")
    # parser.add_argument("-f", metavar = "median_norm_emission", type = str, nargs = 1, help = "Plot median emission normalized per marker.")
    parser.add_argument("-m", metavar = "state_membership", type = str, nargs = 1, help = "Plot histogram of state memberships.")
    parser.add_argument("-l", metavar = "state_length", type = str, nargs = 1, help = "Plot state length distribution.")
    parser.add_argument("-s", metavar = "states", type = str, nargs = 1, help = "Column name with state sequence.")
    args = parser.parse_args()

    try:
        data = args.c[0]
        model = args.j[0]
        transitionMatrix = args.t[0]
        meanEmission = args.e[0]
        normEmission = args.n[0]
        # medianEmission = args.d[0]
        # medianNormEmission = args.f[0]
        stateMembership = args.m[0]
        stateLength = args.l[0]
        column = args.s[0]
    except:
        parser.print_help()
        return

    HMM = dict()
    with open(model) as file:
        HMM = json.load(file)
    HMM['states'] = int(HMM['states'])
    HMM['methylation'] = True if HMM['methylation'] == "true" else False
    HMM['initial_state_distribution'] = np.array(HMM['initial_state_distribution'], dtype=float)
    HMM['transition_matrix'] = np.array(HMM['transition_matrix'], dtype=float)

    m = len(HMM['marker'])
    data = pd.read_csv(data, sep='\t', converters={0:str})
    data.columns = ['chr', 'start', 'end'] + list(data.columns)[3:]
    max = int(math.ceil(np.percentile(data.iloc[:,3:3+m].values, 99.5) / 100.0)) * 100
    
    if HMM['methylation']:
        data['DNA-Methylation'] = data['Meth'] / data['Cov']
        data['DNA-Methylation'] = data['DNA-Methylation'].fillna(-0.005)
        data = data.drop(columns=['Meth', 'Cov'])
        columns = list(data.columns)
        new_cols = columns[:m+3] + ['DNA-Methylation'] + columns[m+3:len(columns)-1]
        data=data[new_cols]


    start_states = range(0, HMM['states'])
    end_states = range(0, HMM['states'])

    if 'topology' in HMM:
        start_states = [int(HMM['topology'][str(i)][0]) for i in range(1, HMM['states']+1)]
        end_states = [int(HMM['topology'][str(i)][-1]) for i in range(1, HMM['states']+1)]

    transition_matrix = np.zeros((HMM['states'], HMM['states']))
    for i in range(HMM['states']):
        for j in range(HMM['states']):
            if i == j:
                transition_matrix[i, j] = HMM['transition_matrix'][end_states[i], end_states[i]]
            else:
                transition_matrix[i, j] = HMM['transition_matrix'][end_states[i], start_states[j]]

    plots.set_plot_style()
    plots.plot_mean_emission(data.iloc[:, 3:], column, m, HMM['states'], meanEmission)
    # plots.plot_median_emission(data.iloc[:, 3:], column, m, HMM['states'], medianEmission)
    plots.plot_state_memberships(data, column, HMM, stateMembership)
    plots.plot_transition(transition_matrix, transitionMatrix)
    plots.plot_state_length(data, column, stateLength)
    plots.plot_mean_emission_norm(data.iloc[:, 3:], column, m, HMM['states'], normEmission)
    # plots.plot_median_emission_norm(data.iloc[:, 3:], column, m, HMM['states'], medianNormEmission)


    if HMM['methylation']:
        file = meanEmission[:-16]
        plots.plot_average_methylation(data, column, file+'averageMethylation.png')

if __name__ == "__main__":
    main()
